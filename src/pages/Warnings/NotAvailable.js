import React from 'react';

const NotAvailable = () => {
  return (
    <div className="container">
      <br/><br/><br/><br/><br/>
      <p>
        Este equipamento se encontra indisponível no momento.
      </p>
      <p>
        Calma, nós vamos te levar para escolher outro produto
      </p>
      Link para voltar
    </div>
  );
};

export default NotAvailable;